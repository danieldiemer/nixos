if status is-interactive
	# Commands to run in interactive sessions can go here
    	set fish_greeting 	 	#suppres default intro
    	set TERM "xterm-256color"

	export VISUAL=nvim
	export EDITOR=nvim

	### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
	set fish_color_normal '#657b83'
	set fish_color_autosuggestion '#586e75'
	set fish_color_command '#859900'
	set fish_color_error '#dc322f'
	set fish_color_param '#268bd2'

	# Some System Utilities
	alias sus='systemctl suspend'
	alias off='systemctl poweroff'
	alias reb='systemctl reboot'
	alias c='clear'

	# Show Weather
	alias wttr='curl "wttr.in/Burnet?u"'

	### git helper ###
	alias ga='git add -A && git status'
	alias gs='git status'
	alias gc='git commit -m'
	alias gpsh='git push'
	alias gpll='git pull'
	
	### NixOS ###
	alias nC='sudo nvim /etc/nixos/configuration.nix'
	alias nRS='sudo nixos-rebuild switch'
	alias nRB='sudo nixos-rebuild boot'


	### Package Commands ###
	alias ref_up='sudo reflector --verbose --country "United States" --latest 20 --fastest 10 --protocol https --sort rate --save /etc/pacman.d/mirrorlist'
	alias p='pacman'
	alias pSyu='sudo pacman -Syu'
	alias pRsn='sudo pacman -Rsn'
	alias pS='sudo pacman -S'
	alias pQs='pacman -Qs'   ## search installed
	alias pSs='pacman -Ss'   ## search repositories
	alias pQm='pacman -Qm'   ## list manually installed packages
	alias pSi='pacman -Si'   ## Synced repo online
	alias pQi='pacman -Q -i' ## package information
	alias pQo='pacman -Q -o' ## what package owns command
	alias pBU='source ~/.config/arch/packages/get_pkg_lists.sh'
	alias up='source ~/.config/arch/update_script.sh'

	# make all things NeoVim
	alias vi='nvim'
	alias vim='nvim'

	# navigation
	alias ..='cd ..'
	alias ...='cd ../..'
	alias .3='cd ../../..'
	alias .4='cd ../../../..'
	alias .5='cd ../../../../..'

	# Better Lists
	alias ls='eza -a   --color=always --icons --group-directories-first' 
	alias ll='eza -la  --color=always --icons --group-directories-first'  
	alias lld='eza -la --color=always --icons --sort=date' 

	# Colorize grep output (good for log files)
	alias grep='grep --color=auto'
	alias egrep='egrep --color=auto'
	alias fgrep='fgrep --color=auto'

	# confirm before overwriting something
	alias cp="cp -i"
	alias mv='mv -i'
	alias rm='rm -i'

	### SETTING THE STARSHIP PROMPT ###
	#starship init fish | source

	### SETTING NEW VARIABLES
	#export STARSHIP_CONFIG=/home/djd/.config/starship/starship.toml
	#export PYENV_ROOT="/home/djd/.pyenv"
	export PATH="/home/djd/.local/bin:$PATH"

	### PYENV SETUP
	#pyenv init - | source

	#### NVIM APPIMAGE SETUP ####
	#
	#neofetch --ascii_colors 4 4



	# Generated for envman. Do not edit.
	test -s ~/.config/envman/load.fish; and source ~/.config/envman/load.fish
end
