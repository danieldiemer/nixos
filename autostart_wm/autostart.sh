#!/usr/bin/env bash 

#Monitor Layout
source /home/djd/.config/autostart_wm/3_mon_layout.sh

#Composter & wallpaper
picom &
nitrogen --restore &

#screen locking
#xss-lock physlock &

#System Tools
lxqt-policykit-agent &
udiskie --tray &
#pcmanfm -d &
flameshot &
dunst &
alacritty -e fish &
#flatpak run com.brave.Browser &
#kitty -e cava &
#kitty -e ncspot &
#com.vscodium.codium &
#proton-mail &
#Godot &

#ChatApps
signal-desktop &
#discord &
telegram-desktop &

# Appimages
#/home/djd/Applications/pcloud_f1ac5a70f1f1fe44a0e4180f3f93d61d &

# SNAPS
#accountable2you &

