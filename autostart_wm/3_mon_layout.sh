#!/bin/sh
xrandr --output DisplayPort-0 --primary --mode 3440x1440 --pos 1920x510 --rotate normal --output DisplayPort-1 --mode 1920x1080 --pos 0x1080 --rotate normal --output DisplayPort-2 --mode 1920x1080 --pos 0x0 --rotate normal --output DisplayPort-3 --off --output HDMI-A-0 --off --output HDMI-A-1 --off
