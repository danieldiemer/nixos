from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy


#-------------------------------------VARIABLE DEFINITIONS---------------------
mod         ="mod4"
terminal    ="alacritty -e fish"
config_file ="alacritty -e nvim /home/djd/.config/qtile/config.py"
editor1     ="alacritty -e nvim"
editor2     ="vscodium"
sysmon      ="alacritty -e btop"
browser1    ="qutebrowser"
browser2    ="com.brave.Browser"
rofiRun     ="rofi -show combi -modes combi -combi-modes 'window,run' "
rofiDrun    ="rofi -show combi -modes combi -combi-modes 'window,drun' "
visualfm1   ="alacritty -e nvim ."
visualfm2   ="pcmanfm"
screenshot  ="flameshot gui"
screenlock  ="betterlockscreen -l --dim 40 --blur 1 off 300"
cava        ="kitty -e cava"
ncspot      ="kitty -e ncspot"
lock        ="physlock"

#-------------------------------------CUSTOM HACKS----------------------------
floating_window_index = 0

def float_cycle(qtile, forward: bool):
    global floating_window_index
    floating_windows = []
    for window in qtile.current_group.windows:
        if window.floating:
            floating_windows.append(window)
    if not floating_windows:
        return
    floating_window_index = min(floating_window_index, len(floating_windows) -1)
    if forward:
        floating_window_index += 1
    else:
        floating_window_index += 1
    if floating_window_index >= len(floating_windows):
        floating_window_index = 0
    if floating_window_index < 0:
        floating_window_index = len(floating_windows) - 1
    win = floating_windows[floating_window_index]
    win.cmd_bring_to_front()
    win.cmd_focus()

@lazy.function
def float_cycle_backward(qtile):
    float_cycle(qtile, False)

@lazy.function
def float_cycle_forward(qtile):
    float_cycle(qtile, True)

#-------------------------------------KEYBINDS---------------------------------------------------
#Commands at https://docs.qtile.org/en/latest/manual/config/lazy.html
keys = [

    #Floating Window Commands
    Key([mod, "shift"],            "period",   float_cycle_forward),
    Key([mod, "shift"],             "comma",   float_cycle_backward),
    Key([mod, "shift"],                 "f",   lazy.window.toggle_floating(), desc="Toggle Floating Behavior"),
    Key([mod,        ],                 "f",   lazy.window.toggle_fullscreen(), desc="Toggle fullscreen"),

    # Move Focus
    Key([mod],                          "j",   lazy.layout.down(),  desc="Move focus down"),
    Key([mod],                          "k",   lazy.layout.up(),    desc="Move focus up"),
    Key([mod],                          "n",   lazy.layout.next(),  desc="Move window focus to next window"), #Useful for the MAX layout
    Key([mod,            "shift"],      "j",   lazy.layout.shuffle_down(),  desc="Move window down"),
    Key([mod,            "shift"],      "k",   lazy.layout.shuffle_up(),    desc="Move window up"),
    Key([mod,            "shift"],      "m",   lazy.layout.swap_main(),     desc="Swap to Master"),   
    # Grow/shrink windows.
    Key([mod,                   ],      "h",   lazy.layout.shrink_main(),   desc="Shrink main"),
    Key([mod,                   ],      "l",   lazy.layout.grow_main(),     desc="Grow main"),
    Key([mod],                     "period",   lazy.prev_screen(),          desc="prev screen"),
    Key([mod],                      "comma",   lazy.next_screen(),          desc="next screen"),
    Key([mod,             "shift"],     "n",   lazy.layout.normalize(),     desc="Reset all window sizes"),
    # Layout Manipulations
    Key([mod],                        "Tab",   lazy.next_layout(),          desc="Toggle between layouts"),
    # Manage Things
    Key([mod],                          "z",   lazy.spawn(),                desc="lock"),    
    Key([mod],                          "c",   lazy.window.kill(),          desc="Kill focused window"),
    Key([mod, "control"         ],      "r",   lazy.reload_config(),        desc="Reload the config"),
    Key([mod, "control"         ],      "q",   lazy.shutdown(),             desc="Shutdown Qtile"),
    # Launch Things
    Key([mod],                          "Return",   lazy.spawn(terminal),   desc="Launch terminal"),
    Key([mod, "shift"           ],      "Return",   lazy.spawn(sysmon),     desc="Launch sys mon"),
    Key([mod],                          "i",   lazy.spawn(editor1)),
    Key([mod],                          "y",   lazy.spawn(cava)),
    Key([mod, "shift"           ],      "y",   lazy.spawn(ncspot)) ,
    Key([mod],                          "b",   lazy.spawn(browser1)),
    Key([mod, "shift"           ],      "b",   lazy.spawn(browser2)) ,
    Key([mod, "shift"           ],      "i",   lazy.spawn(config_file)) ,
    Key([mod,                   ],      "u",   lazy.spawn(rofiRun)) ,
    Key([mod, "shift"           ],      "u",   lazy.spawn(rofiDrun)) ,
    Key([mod,                   ],      "o",   lazy.spawn(visualfm1)) ,
    Key([mod, "shift"           ],      "o",   lazy.spawn(visualfm2)) ,
    Key([mod, "shift", "control"], "Return",   lazy.spawncmd(),             desc="Spawn a command using a prompt widget"),
    Key([mod, "shift",          ],      "p",   lazy.spawn(lock),            desc="Screen + tty locker"),

    # Sound using wireplumber (pipewire)
    Key([], "XF86AudioMute", lazy.spawn("wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"),desc="volume mute"),
    Key([], "XF86AudioLowerVolume", lazy.spawn("wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"),desc="volume raise"),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+"),desc="volume lower"), 
    Key([], "XF86AudioMicMute", lazy.spawn("wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle"),desc="mic mute"), 
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc="Play/Pause player"),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous"), desc="Skip to previous"),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc="Skip to next"),
    # Screenshot
    Key([], "Print", lazy.spawn(screenshot)),

]

#-----------------------------------GROUP SETUP-----------------------------------------
groups = [
        Group("1",
            label="1-TERM",
            layout="monadtall",),
        Group("2",
            label="2-WWW",
            matches=[
                Match(wm_class=["Brave-browser"]),
                Match(wm_class=["brave-browser"]),
                Match(wm_class=["proton mail"]),
                Match(wm_class=["Proton Mail"]),
            ]),   
        Group("3",
            layout="monadwide",
            label="3-MUSIC",
            matches=[
                Match(wm_class=["kitty"]),
            ]),   
        Group("4",
            label="4-CHAT",
            layout="monadtall", 
            matches=[
                Match(wm_class=["telegram-desktop"]),
                Match(wm_class=["TelegramDesktop"]),
                Match(wm_class=["signal"]),
                Match(wm_class=["discord"]),
            ]),  
        Group("5",
            label="5-DEV",
            layout="monadtall",
            matches=[
                Match(wm_class=["vscodium"]),
                Match(wm_class=["VSCodium"]),
                Match(wm_class=["Godot_Editor"]),
                Match(wm_class=["Godot"]),
            ]),
        Group("6",
            label="6-EDIT",
            layout="monadtall",
            matches=[
                Match(wm_class=["kdenlive"]),
                Match(wm_class=["gimp"]),
            ]),  
         Group("7",
            label="--- 7 ---",
            layout="monadtall",),
        Group("8",
            label="8-GAME",
            layout="max",
            matches=[
                Match(wm_class=["steamwebhelper"]),
                Match(wm_class=["steam"]),
            ]),
        Group("9",
            label="9-SYS",
            layout="max", 
            matches=[
                Match(wm_class=["TeamViewer"]),
                Match(wm_class=["pcloud"]),
                Match(wm_class=["Accountable2You"]),
                Match(wm_class=["bitwarden"]),
                Match(wm_class=["Bitwarden"]),
            ]),
        ]

for i in groups:
    keys.extend(
        [Key([mod],i.name,lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name),),
         Key([mod, "shift"],i.name,lazy.window.togroup(i.name, switch_group=False),
            desc="Move to Group".format(i.name),),])

#-----------------------------------LAYOUT SETUP-----------------------------------------
layouts = [
    layout.MonadTall(
        border_width        = 5,
        border_focus        ='#6c71c4',
        border_normal       ='#002b36',
        ratio               = 0.5,
        margin              = 5,
    ),
    layout.MonadWide(
        border_width        = 5,
        border_focus        ='#6c71c4',
        border_normal       ='#002b36',
        ratio               = 0.5,
        margin              = 5,
    ),
#    layout.MonadThreeCol(
#        border_width        = 5,
#        border_focus        ='#6c71c4',
#        border_normal       ='#002b36',
#        ratio               = 0.35,
#        margin              = 5,
#        main_centered       = True,
#        new_client_position ='after_current',
#    ),    
    layout.Max(
        border_width        = 5,
        border_focus        ='#6c71c4',
        border_normal       ='#002b36',
        margin              = 0,
        )
]
#-----------------------------------SCREENS & BAR SETUP-----------------------------------------
widget_defaults = dict(
    font="DaddyTimeMono",
   fontsize=13,
    padding=1,
    foreground='#839496'
)
extension_defaults = widget_defaults.copy()
screens = [
    Screen(
        # Main Monitor
        top=bar.Bar(
            [   
                widget.Clock(
                    background='#073642',
                    padding=22,
                    foreground      = '#268bd2',
                    fontsize=16,
                    format="%A %B %d   %H:%M:%S  "),
                widget.GroupBox(
                    hide_unused=True,
                    active='#93a1a1',
                    highlight_method='line',
                    block_highlight_text_color='#eee8d5',
                    highlight_color='#2aa198',
                    use_mouse_wheel=False,
                    disable_drag=True,
                    other_screen_border='#dc322f',
                    other_current_screen_border='#6c71c4',
                    this_screen_border='#2aa198',
                    this_current_screen_border='#2aa198',
                ),
                widget.Spacer(length=20),
                widget.CurrentLayout(
                    fontsize=16,
                    background='#073642',
                    padding=10,
                    foreground='#6c71c4'
                ),
                widget.Spacer(length=20),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Prompt(),
                widget.Volume(
                    fmt = '🔊 Vol: {} ',
                    setp='5',
                ),          
                widget.Spacer(length=20),
                widget.Systray(padding = 3),
                widget.Spacer(length=20),
                widget.Wttr(
                    fontsize=16,
                    location={'Burnet':'Burnet, TX '},
                    #TEST with curl -s wttr.in/Burnet
                    format='  | %c %C @ %l| 🌡%t   🍃%w   💧%p | Sunrise %S | Twilight %d | ' ,
                    units='u',
                    update_interval=30,
                ),
                widget.Spacer(length=20),
                widget.DF(
                    fontsize=16,
                    update_interval = 60,
                    foreground = '#6c71c4',
                    partition = '/',
                    format = '{uf}{m}',
                    fmt = '🖴  Disk Free: {}',
                    visible_on_warn = False,
                    ),
                widget.Spacer(length=20),
                widget.Spacer(length=20),
            ],
            40,
            background="#002b36"
        ),
    ),
        #Second Monitor
        Screen(
        top=bar.Bar(
            [   
                 widget.Clock(
                    background='#073642',
                    padding=20,
                    foreground      = '#268bd2',
                    fontsize=16,
                    format="%A %B %d   %H:%M:%S  "),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                 widget.GroupBox(
                    hide_unused=True,
                    active='#93a1a1',
                    highlight_method='line',
                    block_highlight_text_color='#eee8d5',
                    highlight_color='#2aa198',
                    use_mouse_wheel=False,
                    disable_drag=True,
                    other_screen_border='#dc322f',
                    other_current_screen_border='#6c71c4',
                    this_screen_border='#2aa198',
                    this_current_screen_border='#2aa198',
                ),
                 widget.CurrentLayout(
                    fontsize=14,
                    background='#073642',
                    padding=10,
                    foreground='#6c71c4',
                ),
                widget.Prompt(),
                #widget.StatusNotifier(),            
                #widget.Systray(padding = 3),
                widget.Spacer(length=20),
                widget.TextBox("1", name="Screen #"),
                widget.Spacer(length=5),
            ],
            35,
            background="#002b36"
        ),
    ),
        #Third Monitor
        Screen(
        top=bar.Bar(
            [   
                widget.Clock(
                    background='#073642',
                    padding=20,
                    foreground      = '#268bd2',
                    fontsize=16,
                    format="%A %B %d   %H:%M:%S  "),
                widget.Spacer(length=20),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.GroupBox(
                    hide_unused=True,
                    active='#93a1a1',
                    highlight_method='line',
                    block_highlight_text_color='#eee8d5',
                    highlight_color='#2aa198',
                    use_mouse_wheel=False,
                    disable_drag=True,
                    other_screen_border='#dc322f',
                    other_current_screen_border='#6c71c4',
                    this_screen_border='#2aa198',
                    this_current_screen_border='#2aa198',
                ),
                  widget.CurrentLayout(
                    fontsize=14,
                    background='#073642',
                    padding=10,
                    foreground='#6c71c4',
                ),               
                widget.Prompt(),
                #widget.StatusNotifier(),            
                #widget.Systray(padding = 3),
                widget.Spacer(length=20),
                widget.TextBox("2", name="Screen #"),
                widget.Spacer(length=5),
            ],
            35,
            background="#002b36"
        ),
    ),
]

#-----------------------------------MOUSE BEHAVIORS-----------------------------------------
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

#-----------------------------------WINDOW BEHAVIOR -----------------------------------------
dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = False
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        #Match(wm_class="TeamViewer"),  # gitk        
        Match(title="TradeStation Login"),  #motivewave
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry 
    ]
)
auto_fullscreen = True
focus_on_window_activation = "urgent"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

no_float_rules = [
    Match(wm_class="pcloud", role="browser-window"),
    Match(wm_class="Accountable2You", role="browser-window"),
]

@hook.subscribe.client_managed
def force_tile(window):
    for rule in no_float_rules:
        if rule.compare(window):
            window.floating = False
            break
#-----------------------------------STARTUP ONCE HOOK-----------------------------------------
from pathlib import Path
import subprocess

@hook.subscribe.startup_once
def autostart():
    home = Path('~/.config/autostart_wm/autostart.sh').expanduser()
    subprocess.run(home)


wmname = "qtile"



